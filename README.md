# Setup AWS account and access
- Create / login into AWS Account
- Open `My Security Credentials`
- Click on `Users`
- Add new user with name `adminuser` and `Programmatic access`
- Create a Group with `AdministratorAccess` with the name `Administrator`
- Then make sure to keep the new credentials

# Deployment with Serverless
### Installation / configuration
```sh
$ npm install -g serverless
$ export AWS_ACCESS_KEY_ID=<your-key-here>
$ export AWS_SECRET_ACCESS_KEY=<your-secret-key-here>
```
### Deploy on AWS
All the service
```sh
$ sls deploy
```
Only the function (faster). You can't use this method if the package is larger than 65Mb.
```sh
$ serverless deploy function -f ProcessDynamoDBStream
```
### Remove from AWS
```sh
$ sls remove
```
### Notes
How to create a new service:
```sh
$ serverless create --template aws-python3 --path test-service
```
See function logs
```sh
$ serverless logs -f ProcessDynamoDBStream
$ serverless logs -f ProcessDynamoDBStream -t
```

# ASW CLI (Manual configuration without Serverless)
### Install
```sh
$ virtualenv venv
$ source venv/bin/activate
(venv) $ pip install awscli
```
### Quick Configuration
```sh
(venv) $ aws configure
```
- Enter the creds for `adminuser` created earlier
- Region name: `us-east-2`
- Output format: `json`

# Lambda function
### Create the Execution Role
Create the following file named `lambda-policy.json`
```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {"Service": "lambda.amazonaws.com"},
            "Action": "sts:AssumeRole"
        }
    ]
}
```

```sh
(venv) $ aws iam create-role --role-name lambda-dynamodb-execution-role --assume-role-policy-document file://lambda-policy.json
(venv) $ aws iam attach-role-policy --role-name lambda-dynamodb-execution-role --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaDynamoDBExecutionRole
```

### Create lambda function
Create the following file named `ProcessDynamoDBStream.py`
```
from __future__ import print_function

def lambda_handler(event, context):
    for record in event['Records']:
        print(record['eventID'])
        print(record['eventName'])       
    print('Successfully processed %s records.' % str(len(event['Records'])))
```
Zip the file
```sh
(venv) $ zip ProcessDynamoDBStream.zip ProcessDynamoDBStream.py
```

Edit the following command before executing it. Change the `role arn` with yours.
```
(venv) $ aws lambda create-function \
--region us-east-2 \
--function-name ProcessDynamoDBStream \
--zip-file fileb://ProcessDynamoDBStream.zip \
--role arn:aws:iam::809185718546:role/lambda-dynamodb-execution-role \
--handler ProcessDynamoDBStream.lambda_handler \
--runtime python3.6
```

# DynamoDB
Create an example table with stream enabled
```sh
(venv) $ aws dynamodb create-table --table-name MusicCollection --attribute-definitions AttributeName=Artist,AttributeType=S AttributeName=SongTitle,AttributeType=S --key-schema AttributeName=Artist,KeyType=HASH AttributeName=SongTitle,KeyType=RANGE --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 --stream-specification StreamEnabled=true,StreamViewType="NEW_IMAGE"
```
Attach stream to the AWS Lambda
Edit the following command before executing it. Change the `event-source` with the stream arn.
```sh
(venv) $ aws lambda create-event-source-mapping \
--region us-east-2 \
--function-name ProcessDynamoDBStream \
--event-source arn:aws:dynamodb:us-east-2:809185718546:table/MusicCollection/stream/2018-07-26T13:15:57.552 \
--batch-size 100 \
--starting-position TRIM_HORIZON
```

# Emulambda
For executing locally the python lambda function

```sh
$ source venv/bin/activate
(venv) $ git clone https://github.com/fugue/emulambda.git
(venv) $ pip install -e emulambda
```

### Testing lambda function with `fake` stream data
The data are located in the `event-stream-sample.ldjson` file

```sh
(venv) $ emulambda ProcessDynamoDBStream.lambda_handler - -s -v < event-stream-sample.ldjson
```