from __future__ import print_function

import boto3
import json


def lambda_handler(event, context):
    for record in event['Records']:
        print(record['eventID'])
        print(record['eventName'])
        if record['eventName'] != 'INSERT':
            create_item()
    print('Successfully processed %s records.' % str(len(event['Records'])))


def create_item():
    dynamodb = boto3.resource('dynamodb', region_name='us-east-2')
    table = dynamodb.Table('MusicCollection')

    response = table.put_item(
        Item={
            'Artist': 'Bono',
            'SongTitle': 'Without you'
        }
    )

    print("PutItem succeeded:")
    print(json.dumps(response, indent=4))
